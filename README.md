# CA-Manager

Management utility for self-hosted certificate authorities.

## Todo

- [ ] CA
    - [x] Initialize
        - [x] RSA key pairs
        - [x] ed25519 key pairs
        - [x] Encrypted private key
        - [x] Certificate revocation list
        - [x] Intermediate certificate authorities
    - [x] Get files
    - [ ] Renew certificate
    - [ ] Edit subject data
- [x] Create new certificates
    - [x] RSA key pairs
    - [x] ed25519 key pairs
    - [x] Encrypted private keys
- [x] Get files
    - [x] Decrypt encrypted keys
    - [x] Output formats
    - [x] Print certificates in text form
- [ ] Edit
    - [ ] Subject data
    - [ ] Subject alternative names
- [ ] Renew certificates
- [ ] Revoke certificates

## Architecture

```text
<CA_STORE>/
    config.toml
    ca/
        privkey.pem (CA Private Key)
        cert.pem (CA Certificate)
        chain.pem (If intermediate, contains the root cert/chain and the ca cert)
    <NAME>/
        privkey.pem (Private Key)
        cert.pem (Certificate)
        chain (ca cert/chain and the cert)
    <NAME>/
        ...
        <NAME>/
            ...
```

## Interface

```sh
$ ca-manager [GLOB_ARGS] <COMMAND>
```

`GLOB_ARGS`:

```text
    --store <DIR>  Use an alternative store
```

`<COMMAND>`:

```text
    ca     <CA_ARGS>
    new    <NEW_ARGS>    <NAME> Generate a new keypair and certificate
    get    [GET_ARGS]    <NAME> Get the certificate
    renew  <EXPIRES>     <NAME> Renew the certificate
    revoke               <NAME> Revoke the certificate
    edit   [EDIT_ARGS]   <NAME> Edit the subject data or subject alternative
                                names for the certificate if no args the edits
                                interactivly
```

`<CA_ARGS>`:

```text
    init              Initialize a new ca store
        [intermediate]     Define this ca as an intermediate
          --ca-key  <CA_KEY>       Path to the ca key
          --ca-cert <CA_CERT>      Path to the ca certificate
          --chain   [CHAIN]        Path to the ca certificate chain
        <NEW_ARGS_GEN>
    get               Get the ca certificate
        --chain   Get the ca certificate chain
        --crl     Get the certificate revocation list
        --key     Get the ca private key
        --cert    Get the ca certificate
        --format  Output Format
        --text    Print out the certificate(s) in text form
        --decrypt Decrypt the private key
    renew <EXPIRES>   Renew the ca certificate
    edit  [EDIT_ARGS] Edit subject data or subject alternative names
```

`<NEW_ARGS_GEN>`:

```text
    --x509        <SUBJ_DATA> X509 Subject Data or path to file containing it
    --algorithm   <ALGORITHM> Which algorithm to use for the generated keypair
    --bits        <BITS>      The number of bits for a Algorithm::RSA keypair [default: 4096]
    --expires     <EXPIRES>
    --encrypt                 Encrypt the private key
```

`<NEW_ARGS>`:

```text
    <NEW_ARGS_GEN>
    --subject_alt <SUBJ_ALT>
```

`[GET_ARGS]`:

```text
    --chain   Get the certificate chain
    --key     Get the private key
    --cert    Get the certificate
    --format  Output Format
    --text    Print out the certificate(s) in text form
    --decrypt Decrypt the private key
```

`[EDIT_ARGS]`:

```text
    --x509 <SUBJ_DATA> X509 Subject Data or path to file containing it
```

### Definitions

`<ALGORITHM>`:

- RSA
    - Use the RSA Algorithm, with the specified number of bits
- ED25519
    - Use the ED25519 Algorithm

`<EXPIRES>`:

One of:

- [n]d - N Days
- [n]m - N Months
- [n]y - N Years

`<SUBJ_DATA>`:

Either a semi-colon separated list of subject data or path to file containing
the subject data. The format is as follows:

```
C="US";ST="Utah";L="Lehi";O="Your Company, Ltd";OU="IT";CN="example.com"
```

The fields `C`, `ST`, `O` and `CN` are required, all others are optional.

`<SUBJ_ALT>`:

Either a comma separated list of subject alternative names or path to file
containing subject alternative names. The format is as follows:

```
DNS:example.com,IP:1.2.3.4
```

A detailed explanation with all available option can be found [here](https://www.openssl.org/docs/man1.0.2/man5/x509v3_config.html#Subject-Alternative-Name).

## Notes

`CHANGELOG.git.md` is built with [`changelog`](https://gitlab.com/Th3-Wr41th/changelog)

## License

This project is Licensed under the [Mozilla Public License 2.0](LICENSE)

## Source

https://github.com/ChristianLempa/cheat-sheets/blob/main/misc/ssl-certs.md
