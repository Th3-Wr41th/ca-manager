// SPDX-License-Identfier: MPL-2.0

use std::{collections::HashMap, env, ffi::OsStr, fs, path::Path, process::Stdio, rc::Rc};

use chrono::{DateTime, Days, Months, Utc};
use color_eyre::eyre::{self, bail, eyre, Context as _, ContextCompat as _, Report};
use openssl::{nid::Nid, x509::X509Name};
use regex::Regex;

pub fn try_open<P: AsRef<OsStr>>(path: P) -> eyre::Result<()> {
    if let Some(val) = env::var_os("EDITOR") {
        let mut cmd = open::with_command(path, val.to_string_lossy());

        let status = cmd
            .stdin(Stdio::inherit())
            .stdout(Stdio::inherit())
            .stderr(Stdio::null())
            .status();

        status.context("failed to open path")?;
    } else {
        let mut last_err: Option<eyre::Result<()>> = None;

        for mut cmd in open::commands(path) {
            let status = cmd
                .stdin(Stdio::inherit())
                .stdout(Stdio::inherit())
                .stderr(Stdio::null())
                .status();

            match status {
                Ok(status) if status.success() => return Ok(()),
                Ok(status) => {
                    last_err = Some(Err(eyre!("Launcher {cmd:?} failed with {status:?}")));
                }
                Err(err) => last_err = Some(Err(eyre!(err))),
            }
        }

        if let Some(err) = last_err {
            err.context("No launcher worked, at least one error")?;
        }
    }

    Ok(())
}

pub fn parse_x509_name_data(data: &str) -> eyre::Result<X509Name> {
    let data: Rc<str> = if Path::new(data).exists() {
        let bytes = fs::read(data).context("failed to read file")?;

        String::from_utf8(bytes)?.into()
    } else {
        data.into()
    };

    let fields_iter = data.split(';');

    let mut fields: HashMap<Nid, &str> = HashMap::new();

    for field in fields_iter {
        let Some((key, value)) = field.split_once('=') else {
            continue;
        };

        let nid = match key {
            "C" => Nid::COUNTRYNAME,
            "ST" => Nid::STATEORPROVINCENAME,
            "L" => Nid::LOCALITYNAME,
            "O" => Nid::ORGANIZATIONNAME,
            "OU" => Nid::ORGANIZATIONALUNITNAME,
            "CN" => Nid::COMMONNAME,
            _ => continue,
        };

        fields.insert(nid, value);
    }

    validate_fields(&fields)?;

    let mut builder = X509Name::builder()?;

    for (nid, value) in &fields {
        builder.append_entry_by_nid(*nid, value.replace(['\'', '\"'], "").trim())?;
    }

    Ok(builder.build())
}

fn validate_fields(fields: &HashMap<Nid, &str>) -> eyre::Result<()> {
    let mut missing: Option<Report> = None;

    if !fields.contains_key(&Nid::COMMONNAME) {
        let msg = "missing common name (CN)";

        missing = Some(missing.map_or(eyre!(msg), |err| err.wrap_err(msg)));
    }

    if !fields.contains_key(&Nid::ORGANIZATIONNAME) {
        let msg = "missing organisation name (O)";

        missing = Some(missing.map_or(eyre!(msg), |err| err.wrap_err(msg)));
    }

    if !fields.contains_key(&Nid::STATEORPROVINCENAME) {
        let msg = "missing state/province name (ST)";

        missing = Some(missing.map_or(eyre!(msg), |err| err.wrap_err(msg)));
    }

    if !fields.contains_key(&Nid::COUNTRYNAME) {
        let msg = "missing country name (C)";

        missing = Some(missing.map_or(eyre!(msg), |err| err.wrap_err(msg)));
    }

    if let Some(report) = missing {
        let err = report.wrap_err("missing required values");

        Err(err)
    } else {
        Ok(())
    }
}

pub fn parse_expiry_timespan(time: &str) -> eyre::Result<u32> {
    let re = Regex::new(r"(?<time>[\d]+)(?<unit>\w+)")?;

    let captures = re.captures(time).context("invalid timespan")?;

    let time = &captures["time"];

    let unit = match &captures["unit"] {
        "d" => Unit::Day,
        "m" => Unit::Month,
        "y" => Unit::Year,
        _ => bail!("invalid unit component"),
    };

    let days = unit.to_days(time)?;

    Ok(days)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Unit {
    Day,
    Month,
    Year,
}

impl Unit {
    fn to_days(self, time: &str) -> eyre::Result<u32> {
        let now: DateTime<Utc> = Utc::now();

        let time: u32 = time.parse().context("invalid time component")?;

        let future: DateTime<Utc> = match self {
            Self::Day => {
                let days = Days::new(u64::from(time));

                now + days
            }
            Self::Month => {
                let months = Months::new(time);

                now + months
            }
            Self::Year => {
                let as_months = time * 12;

                let months = Months::new(as_months);

                now + months
            }
        };

        let delta = future - now;

        let days: u32 = delta
            .num_days()
            .try_into()
            .context("time to far into the future")?;

        Ok(days)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use rstest::rstest;

    #[rstest]
    #[case("1d", 1)]
    #[case("12345d", 12345)]
    #[case("12m", 365)]
    // Note: these cases may fail when in a leap year
    #[case("1y", 365)]
    #[case("100y", 36524)]
    fn test_timespan_parsing(#[case] input: &str, #[case] expected: u32) {
        let res = parse_expiry_timespan(input);

        assert!(res.is_ok());

        assert_eq!(res.unwrap(), expected);
    }
}
