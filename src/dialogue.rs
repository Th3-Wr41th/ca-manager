// SPDX-License-Identfier: MPL-2.0

use dialoguer::{console::Term, theme::ColorfulTheme, Confirm, Password};
use once_cell::sync::Lazy;

pub static TERM: Lazy<Term> = Lazy::new(Term::stderr);
pub static THEME: Lazy<ColorfulTheme> = Lazy::new(ColorfulTheme::default);

pub trait ConfirmExt {
    fn default_prompt(prompt: &str) -> Self;
}

impl ConfirmExt for Confirm<'_> {
    fn default_prompt(prompt: &str) -> Self {
        Self::with_theme(&*THEME)
            .with_prompt(prompt)
            .default(false)
            .report(false)
    }
}

pub trait PasswordExt {
    fn default_ext() -> Self;
}

impl PasswordExt for Password<'_> {
    fn default_ext() -> Self {
        Self::with_theme(&*THEME).report(false)
    }
}
