// SPDX-License-Identfier: MPL-2.0

use std::{env, sync::Arc};

use clap::{Parser, Subcommand};
use resolve_path::PathResolveExt as _;

use crate::subcmds::{ca::CaCommands, get::GetArgs, new::NewArgs, renew::RenewArgs};

#[derive(Debug, Clone, Parser)]
#[command(version, about, long_about = None)]
pub struct CliArgs {
    /// Path to alternative store location
    #[arg(long, default_value_t = CliArgs::default_store())]
    pub store: Arc<str>,
    #[command(subcommand)]
    pub command: Command,
}

#[derive(Debug, Clone, Subcommand)]
pub enum Command {
    /// Manipulate the certificate authority
    Ca {
        #[command(subcommand)]
        command: CaCommands,
    },
    /// Generate a new keypair and certificate
    New {
        #[command(flatten)]
        args: NewArgs,

        /// Name of certificate with optional sub directories
        name: Arc<str>,
    },
    /// Get a certificate
    Get(GetArgs),
    /// Edit the subject data for a certificate
    Edit {
        /// Name of certificate with optional sub directories
        name: Arc<str>,
    },
    /// Renew a certificate
    Renew {
        #[command(flatten)]
        args: RenewArgs,

        /// Name of certificate with optional sub directories
        name: Arc<str>,
    },
    /// Revoke a certificate
    Revoke {
        /// Name of certificate with optional sub directories
        name: Arc<str>,
    },
    /// Check the health of a certificate
    Check {
        /// Name of certificate with optional sub directories
        name: Arc<str>,
    },
}

impl CliArgs {
    fn default_store() -> Arc<str> {
        let dir: Arc<str> = if let Some(var) = env::var_os("XDG_DATA_HOME") {
            var.to_string_lossy().into()
        } else {
            let dir = match env::var("HOME") {
                Ok(val) => format!("{val}/.local/share"),
                Err(_err) => "~/.local/share".resolve().to_string_lossy().into(),
            };

            dir.into()
        };

        format!("{dir}/ca-store").into()
    }
}
