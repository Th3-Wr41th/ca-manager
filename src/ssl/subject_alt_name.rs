// SPDX-License-Identfier: MPL-2.0

use std::rc::Rc;

use color_eyre::eyre::{self, ContextCompat as _};
use openssl::{
    nid::Nid,
    x509::{extension::SubjectAlternativeName, X509Ref},
};

#[derive(Debug, Clone)]
enum GeneralName {
    Dns(Rc<str>),
    Email(Rc<str>),
    Uri(Rc<str>),
    Ip(Rc<str>),
    Rid(Rc<str>),
}

#[derive(Debug, Clone, Default)]
pub struct SubjectAltName(Vec<GeneralName>);

impl SubjectAltName {
    pub fn from_cert(cert: &X509Ref) -> eyre::Result<Self> {
        let pos = cert
            .get_extension_by_nid(&Nid::SUBJECT_ALT_NAME)
            .context("certificate does not have subject alternative names extension")?;

        let ext = cert.get_extension(pos)?;

        let data = ext.to_text()?;
        let data = String::from_utf8(data)?;

        Ok(Self::from_str(&data))
    }

    pub fn from_str(data: &str) -> Self {
        let data_iter = data.split(',');

        let mut items: Vec<GeneralName> = Vec::new();

        for value in data_iter {
            let value = value.trim().to_lowercase();
            let value: &str = value.as_str();

            let Some((id, val)) = value.split_once(':') else {
                continue;
            };

            match id {
                "dns" => {
                    let val = GeneralName::Dns(val.into());
                    items.push(val);
                }
                "email" => {
                    let val = GeneralName::Email(val.into());
                    items.push(val);
                }
                "uri" => {
                    let val = GeneralName::Uri(val.into());
                    items.push(val);
                }
                "ip" | "ip address" => {
                    let val = GeneralName::Ip(val.into());
                    items.push(val);
                }
                "rid" | "registered id" => {
                    let val = GeneralName::Rid(val.into());
                    items.push(val);
                }
                "othername" => {
                    warn!("otherName is not supported");
                }
                "dirname" => {
                    warn!("dirName is not supported");
                }
                _ => continue,
            };
        }

        Self(items)
    }

    pub fn as_str(&self) -> Rc<str> {
        let mut output = String::new();

        for (idx, item) in self.0.iter().enumerate() {
            let val = match item {
                GeneralName::Dns(val) => format!("DNS:{val}"),
                GeneralName::Email(val) => format!("email:{val}"),
                GeneralName::Uri(val) => format!("URI:{val}"),
                GeneralName::Ip(val) => format!("IP:{val}"),
                GeneralName::Rid(val) => format!("RID:{val}"),
            };

            output.push_str(&val);

            if idx != (self.0.len() - 1) {
                output.push_str(", ");
            }
        }

        output.into()
    }
}

impl From<SubjectAltName> for SubjectAlternativeName {
    fn from(value: SubjectAltName) -> Self {
        let mut builder = Self::new();

        for item in value.0 {
            let _ = match item {
                GeneralName::Dns(val) => builder.dns(val.as_ref()),
                GeneralName::Email(val) => builder.email(val.as_ref()),
                GeneralName::Uri(val) => builder.uri(val.as_ref()),
                GeneralName::Ip(val) => builder.ip(val.as_ref()),
                GeneralName::Rid(val) => builder.rid(val.as_ref()),
            };
        }

        builder
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[rstest::rstest]
    #[case("DNS:example.com", "...")]
    #[case("IP:1.2.3.4", "...")]
    #[case("IP Address:1.2.3.4", "IP:1.2.3.4")]
    #[case("IP:13::17", "...")]
    #[case("email:my@example.com", "...")]
    #[case("RID:1.2.3.4", "...")]
    #[case("Registered ID:1.2.3.4", "RID:1.2.3.4")]
    #[case("URI:https://example.com", "...")]
    #[case("DNS:example.com, IP:1.2.3.4", "...")]
    fn test_str_parsing(#[case] input: &str, #[case] expected: &str) {
        let alt_name = SubjectAltName::from_str(input);

        let new_str = alt_name.as_str();
        let new_str: &str = new_str.as_ref();

        let expected = if expected == "..." { input } else { expected };

        assert_eq!(expected, new_str);
    }
}
