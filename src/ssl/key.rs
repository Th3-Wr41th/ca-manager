// SPDX-License-Identfier: MPL-2.0

use std::{
    fmt,
    fs::{self, File},
    io::Write as _,
    os::unix::fs::PermissionsExt as _,
    path::Path,
    rc::Rc,
};

use color_eyre::eyre::{self, bail, Context};
use dialoguer::Password;
use once_cell::sync::Lazy;
use openssl::{
    pkey::{Id, PKey, Private},
    rsa::Rsa,
    symm::Cipher,
};

use crate::dialogue::{PasswordExt, TERM};

static CIPHER: Lazy<Cipher> = Lazy::new(Cipher::aes_256_cbc);

/// Public Key Generation Algorithm
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Algorithm {
    /// Generate an RSA key with specified bits
    Rsa(u32),
    /// Generate a ed25519 key
    Ed25519,
}

pub struct KeyPair {
    pub keypair: PKey<Private>,
    pub algo: Algorithm,
}

impl KeyPair {
    /// Generate a new keypair
    pub fn generate(algo: Algorithm) -> eyre::Result<Self> {
        info!("generating keypair: {0:#?}", algo);

        let pkey = match algo {
            Algorithm::Rsa(bits) => {
                let rsa = Rsa::generate(bits).context("failed to generate rsa keypair")?;

                PKey::from_rsa(rsa).context("failed to construct pkey")?
            }
            Algorithm::Ed25519 => {
                PKey::generate_ed25519().context("failed to generate ed25519 keypair")?
            }
        };

        Ok(Self {
            keypair: pkey,
            algo,
        })
    }

    /// Serialize private key to PEM-encoded PKCS#8
    ///
    /// The output will have a header of `-----BEGIN PRIVATE KEY-----`
    pub fn private_key_to_pem_pkcs8(&self) -> eyre::Result<Rc<str>> {
        let pem = self
            .keypair
            .private_key_to_pem_pkcs8()
            .context("failed to serialize to PEM-encoded PKCS#8")?;

        let data =
            String::from_utf8(pem).context("PEM-encoded PKCS#8 contained non utf-8 characters")?;

        Ok(data.into())
    }

    /// Serialize private key to encrypted PEM-encoded PKCS#8
    ///
    /// The output will have a header of `-----BEGIN ENCRYPTED PRIVATE KEY-----`
    pub fn private_key_to_pem_pkcs8_passphrase(&self, pass: &str) -> eyre::Result<Rc<str>> {
        let pem = self
            .keypair
            .private_key_to_pem_pkcs8_passphrase(*CIPHER, pass.as_bytes())
            .context("failed to serialize to encrypted PEM-encoded PKCS#8")?;

        let data = String::from_utf8(pem)
            .context("Encrypted PEM-encoded PKCS#8 contained non utf-8 characters")?;

        Ok(data.into())
    }

    /// Serialize the public key into PEM-encoded structure
    pub fn public_key_to_pem(&self) -> eyre::Result<Rc<str>> {
        let pem = self
            .keypair
            .public_key_to_pem()
            .context("failed to serialize to PEM-encoded PKCS#8")?;

        let data =
            String::from_utf8(pem).context("PEM-encoded PKCS#8 contained non utf-8 characters")?;

        Ok(data.into())
    }

    pub fn write<P: AsRef<Path>>(
        &self,
        path: P,
        encrypt_pass: Option<&str>,
        public_key: bool,
        perms: u32,
    ) -> eyre::Result<()> {
        if public_key {
            let pem = self.public_key_to_pem()?;

            let mut file = File::create(path)?;

            let mut permissions = file.metadata()?.permissions();
            permissions.set_mode(perms);
            file.set_permissions(permissions)?;

            file.write_all(pem.as_bytes())
                .context("failed to write pem data to file")
        } else {
            let pem = if let Some(pass) = encrypt_pass {
                self.private_key_to_pem_pkcs8_passphrase(pass)?
            } else {
                self.private_key_to_pem_pkcs8()?
            };

            let mut file = File::create(path)?;

            let mut permissions = file.metadata()?.permissions();
            permissions.set_mode(perms);
            file.set_permissions(permissions)?;

            file.write_all(pem.as_bytes())
                .context("failed to write pem data to file")
        }
    }

    pub fn load<P: AsRef<Path>>(path: P) -> eyre::Result<(Self, Option<Rc<str>>)> {
        let data = fs::read_to_string(path).context("failed to read key data from file")?;

        let mut encrypt_pass: Option<Rc<str>> = None;

        let key = if data.contains("BEGIN ENCRYPTED PRIVATE KEY") {
            let pass = Password::default_ext()
                .with_prompt("Private Key Password")
                .interact_on(&TERM)
                .context("failed to get private key password from user")?;

            encrypt_pass = Some(pass.clone().into());

            PKey::private_key_from_pem_passphrase(data.as_bytes(), pass.as_bytes())
                .context("failed to deserialize encrypted private key")?
        } else {
            PKey::private_key_from_pem(data.as_bytes())
                .context("failed to deserialize private key")?
        };

        let id = key.id();

        let algo = match id {
            Id::RSA => Algorithm::Rsa(key.bits()),
            Id::ED25519 => Algorithm::Ed25519,
            _ => bail!("unsupported key format"),
        };

        Ok((Self { keypair: key, algo }, encrypt_pass))
    }
}

impl fmt::Debug for KeyPair {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let privkey = self
            .private_key_to_pem_pkcs8()
            .expect("failed to get pem encoded private key");

        let pubkey = self
            .public_key_to_pem()
            .expect("failed to get pem encoded public key");

        writeln!(f, "{privkey}")?;
        writeln!(f, "{pubkey}")
    }
}
