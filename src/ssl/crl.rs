// SPDX-License-Identfier: MPL-2.0

use std::{
    fmt,
    fs::{self, File},
    io::Write,
    os::unix::fs::PermissionsExt,
    path::Path,
    rc::Rc,
};

use color_eyre::eyre::{self, Context};
use openssl::{
    hash::MessageDigest,
    x509::{CrlStatus, X509Crl, X509CrlRef},
};

use super::{Algorithm, Cert, KeyPair};

pub struct Crl(X509Crl);

impl Crl {
    #[allow(dead_code)]
    pub fn inner(&self) -> &X509CrlRef {
        &self.0
    }

    pub fn new(issuer_cert: &Cert, issuer_key: &KeyPair) -> eyre::Result<Self> {
        let mut crl = X509Crl::new(issuer_cert.inner(), None).context("failed to create crl")?;

        let digest = match issuer_key.algo {
            Algorithm::Rsa(_) => MessageDigest::sha256(),
            Algorithm::Ed25519 => MessageDigest::null(),
        };

        crl.sign(&issuer_key.keypair, digest)
            .context("failed to sign crl")?;

        crl.set_next_update_from_now(86400)?; // 24 Hours

        Ok(Self(crl))
    }

    pub fn revoke(&mut self, cert: &Cert) -> eyre::Result<()> {
        let crl = &mut self.0;
        let cert = cert.inner();

        crl.revoke(cert).context("failed to revoke certificate")
    }

    pub fn is_revoked(&self, cert: &Cert) -> CrlStatus {
        self.0.get_by_cert(cert.inner())
    }

    pub fn to_pem(&self) -> eyre::Result<Rc<str>> {
        let data = self.0.to_pem().context("failed to serialize crl to pem")?;

        let pem = String::from_utf8(data)
            .context("pem-encoded crl data contained non-utf8 characters")?;

        Ok(pem.into())
    }

    pub fn to_text(&self) -> eyre::Result<Rc<str>> {
        let data = self
            .0
            .to_text()
            .context("failed to convert crl to human readable text")?;

        let text = String::from_utf8(data).context("data contained non-utf8 characters")?;

        Ok(text.into())
    }

    pub fn write<P: AsRef<Path>>(&self, path: P, perms: u32) -> eyre::Result<()> {
        let pem = self.to_pem()?;

        let mut file = File::create(path)?;

        let mut permissions = file.metadata()?.permissions();
        permissions.set_mode(perms);
        file.set_permissions(permissions)?;

        file.write_all(pem.as_bytes())
            .context("failed to write crl data to file")
    }

    pub fn load<P: AsRef<Path>>(path: P) -> eyre::Result<Self> {
        let data = fs::read(path).context("failed to read file")?;

        let crl = X509Crl::from_pem(&data).context("failed to deserialize pem-encoded crl")?;

        Ok(Self(crl))
    }
}

impl fmt::Debug for Crl {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let text = self
            .to_text()
            .expect("failed to convert crl to human readable text");

        let pem = self.to_pem().expect("failed to serailize crl into pem");

        write!(f, "{text}")?;
        write!(f, "{pem}")
    }
}
