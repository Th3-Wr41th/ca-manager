// SPDX-License-Identfier: MPL-2.0

use std::{
    fmt,
    fs::{self, File},
    io::{BufRead, BufReader, Write},
    ops::Deref,
    os::unix::fs::PermissionsExt,
    path::Path,
    rc::Rc,
};

use color_eyre::eyre::{self, Context as _};
use openssl::{
    asn1::{Asn1Integer, Asn1Time, Asn1TimeRef},
    bn::{BigNum, MsbOption},
    hash::MessageDigest,
    x509::{
        extension::{
            AuthorityKeyIdentifier, BasicConstraints, KeyUsage, SubjectAlternativeName,
            SubjectKeyIdentifier,
        },
        X509Extension, X509NameRef, X509Req, X509VerifyResult, X509,
    },
};

use crate::ssl::Algorithm;

use super::{KeyPair, SubjectAltName};

#[derive(Clone)]
pub struct Cert(X509);

impl Cert {
    pub fn inner(&self) -> &X509 {
        &self.0
    }

    pub fn verify(&self, key: &KeyPair) -> eyre::Result<bool> {
        self.0
            .verify(&key.keypair)
            .context("failed to verify certificate")
    }

    pub fn issued(&self, cert: &Self) -> X509VerifyResult {
        self.0.issued(&cert.0)
    }

    pub fn mk_ca_cert(
        ca_key: &KeyPair,
        x509_name: &X509NameRef,
        not_after: u32,
        intermediate: Option<(&Self, &KeyPair)>,
    ) -> eyre::Result<Self> {
        info!("generating ca cert");

        let KeyPair { keypair, algo } = ca_key;

        let mut cert_builder =
            X509::builder().context("failed to construct builder for X509 certificate")?;
        // Openssl cert version 3 (0x2)
        cert_builder.set_version(2)?;

        let serial = Self::serial()?;

        let not_before = Asn1Time::days_from_now(0)?;
        let not_after = Asn1Time::days_from_now(not_after)?;

        cert_builder.set_serial_number(&serial)?;
        cert_builder.set_subject_name(x509_name)?;

        if let Some((cert, _)) = intermediate {
            cert_builder.set_issuer_name(cert.0.issuer_name())?;
        } else {
            cert_builder.set_issuer_name(x509_name)?;
        }

        cert_builder.set_pubkey(keypair)?;
        cert_builder.set_not_before(&not_before)?;
        cert_builder.set_not_after(&not_after)?;

        let ctx = if let Some((cert, _)) = intermediate {
            cert_builder.x509v3_context(Some(&cert.0), None)
        } else {
            cert_builder.x509v3_context(None, None)
        };

        let subject_key_identifier = SubjectKeyIdentifier::new().build(&ctx)?;

        let authority_key_identifier = AuthorityKeyIdentifier::new()
            .keyid(false)
            .issuer(false)
            .build(&ctx)?;

        let basic_constraints = BasicConstraints::new().critical().ca().build()?;

        let key_usage = KeyUsage::new()
            .critical()
            .key_cert_sign()
            .crl_sign()
            .build()?;

        cert_builder.append_extension(basic_constraints)?;
        cert_builder.append_extension(subject_key_identifier)?;
        cert_builder.append_extension(authority_key_identifier)?;
        cert_builder.append_extension(key_usage)?;

        if let Some((_, key)) = intermediate {
            let digest = match key.algo {
                Algorithm::Rsa(_) => MessageDigest::sha256(),
                Algorithm::Ed25519 => MessageDigest::null(),
            };

            cert_builder.sign(&key.keypair, digest)?;
        } else {
            let digest = match algo {
                Algorithm::Rsa(_) => MessageDigest::sha256(),
                Algorithm::Ed25519 => MessageDigest::null(),
            };

            cert_builder.sign(keypair, digest)?;
        }

        let cert = cert_builder.build();

        Ok(Self(cert))
    }

    pub fn mk_ca_signed_cert(
        ca_cert: &Self,
        ca_key: &KeyPair,
        key_pair: &KeyPair,
        x509_name: &X509NameRef,
        not_after: &Asn1TimeRef,
        subject_alt_name: SubjectAltName,
    ) -> eyre::Result<Self> {
        info!("generating ca signed cert");

        let req =
            Self::mk_request(key_pair, x509_name).context("failed to make certificate request")?;

        let mut cert_builder = X509::builder()?;
        cert_builder.set_version(2)?;

        let serial_number = Self::serial()?;

        let not_before = Asn1Time::days_from_now(0)?;

        cert_builder.set_serial_number(&serial_number)?;
        cert_builder.set_subject_name(req.subject_name())?;
        cert_builder.set_issuer_name(ca_cert.0.issuer_name())?;
        cert_builder.set_pubkey(&key_pair.keypair)?;
        cert_builder.set_not_before(&not_before)?;
        cert_builder.set_not_after(not_after)?;

        let ctx = cert_builder.x509v3_context(Some(&ca_cert.0), None);

        let subject_key_identifier = SubjectKeyIdentifier::new().build(&ctx)?;

        let authority_key_identifier = AuthorityKeyIdentifier::new()
            .keyid(false)
            .issuer(false)
            .build(&ctx)?;

        let basic_constraints = BasicConstraints::new().build()?;

        let key_usage = KeyUsage::new()
            .critical()
            .non_repudiation()
            .digital_signature()
            .key_encipherment()
            .build()?;

        let subject_alt_name: SubjectAlternativeName = subject_alt_name.into();
        let subject_alt_name: X509Extension = subject_alt_name.build(&ctx)?;

        cert_builder.append_extension(basic_constraints)?;
        cert_builder.append_extension(subject_key_identifier)?;
        cert_builder.append_extension(authority_key_identifier)?;
        cert_builder.append_extension(key_usage)?;
        cert_builder.append_extension(subject_alt_name)?;

        let digest = match ca_key.algo {
            Algorithm::Rsa(_) => MessageDigest::sha256(),
            Algorithm::Ed25519 => MessageDigest::null(),
        };

        cert_builder.sign(&ca_key.keypair, digest)?;

        let cert = cert_builder.build();

        Ok(Self(cert))
    }

    pub fn renew_ca_cert(
        cert: &Self,
        key: &KeyPair,
        not_after: &Asn1TimeRef,
    ) -> eyre::Result<Self> {
        info!("renewing ca certificate");

        let old_cert = cert.inner();

        let KeyPair { keypair, algo } = key;

        let mut builder =
            X509::builder().context("failed to initialize builder for X509 certificate")?;
        builder.set_version(old_cert.version())?;

        let not_before = Asn1Time::days_from_now(0)?;

        builder.set_serial_number(old_cert.serial_number())?;
        builder.set_subject_name(old_cert.subject_name())?;
        builder.set_issuer_name(old_cert.issuer_name())?;
        builder.set_pubkey(keypair)?;
        builder.set_not_before(&not_before)?;
        builder.set_not_after(not_after)?;

        let extensions = old_cert.extensions()?;

        for ext in extensions {
            builder.append_extension2(ext)?;
        }

        let digest = match algo {
            Algorithm::Rsa(_) => MessageDigest::sha256(),
            Algorithm::Ed25519 => MessageDigest::null(),
        };

        builder.sign(keypair, digest)?;

        let cert = builder.build();

        Ok(Self(cert))
    }

    pub fn renew_cert(
        ca_cert: &Self,
        ca_key: &KeyPair,
        cert: &Self,
        key_pair: &KeyPair,
        not_after: u32,
    ) -> eyre::Result<Self> {
        info!("renewing certificate");

        let old_cert = cert.inner();

        let req = Self::mk_request(key_pair, old_cert.subject_name())
            .context("failed to make certificate request")?;

        let mut cert_builder = X509::builder()?;
        cert_builder.set_version(old_cert.version())?;

        let not_before = Asn1Time::days_from_now(0)?;
        let not_after = Asn1Time::days_from_now(not_after)?;

        cert_builder.set_serial_number(old_cert.serial_number())?;
        cert_builder.set_subject_name(req.subject_name())?;
        cert_builder.set_issuer_name(ca_cert.0.issuer_name())?;
        cert_builder.set_pubkey(&key_pair.keypair)?;
        cert_builder.set_not_before(&not_before)?;
        cert_builder.set_not_after(&not_after)?;

        let extensions = old_cert.extensions()?;

        for ext in extensions {
            cert_builder.append_extension2(ext)?;
        }

        let digest = match ca_key.algo {
            Algorithm::Rsa(_) => MessageDigest::sha256(),
            Algorithm::Ed25519 => MessageDigest::null(),
        };

        cert_builder.sign(&ca_key.keypair, digest)?;

        let cert = cert_builder.build();

        Ok(Self(cert))
    }

    fn serial() -> eyre::Result<Asn1Integer> {
        let mut serial = BigNum::new()?;
        serial.rand(159, MsbOption::MAYBE_ZERO, false)?;
        Ok(serial.to_asn1_integer()?)
    }

    pub fn mk_request(key_pair: &KeyPair, x509_name: &X509NameRef) -> eyre::Result<X509Req> {
        info!("generating cert request");

        let KeyPair { keypair, algo } = key_pair;

        let mut req_builder = X509Req::builder()?;
        req_builder.set_pubkey(keypair)?;

        req_builder.set_subject_name(x509_name)?;

        let digest = match algo {
            Algorithm::Rsa(_) => MessageDigest::sha256(),
            Algorithm::Ed25519 => MessageDigest::null(),
        };

        req_builder.sign(keypair, digest)?;

        Ok(req_builder.build())
    }

    pub fn to_pem(&self) -> eyre::Result<Rc<str>> {
        let pem = self
            .0
            .to_pem()
            .context("failed to serialize the certificate into PEM-encoded X509")?;

        let data =
            String::from_utf8(pem).context("PEM-encoded X509 contains non utf-8 characters")?;

        Ok(data.into())
    }

    pub fn to_text(&self) -> eyre::Result<Rc<str>> {
        let text = self
            .0
            .to_text()
            .context("failed to serialize the certificate into human readable text")?;

        let data = String::from_utf8(text).context("text contains non utf-8 characters")?;

        Ok(data.into())
    }

    pub fn write<P: AsRef<Path>>(&self, path: P, perms: u32) -> eyre::Result<()> {
        let pem = self.to_pem()?;

        let mut file = File::create(path)?;

        let mut permissions = file.metadata()?.permissions();
        permissions.set_mode(perms);
        file.set_permissions(permissions)?;

        file.write_all(pem.as_bytes())
            .context("failed to write key data to file")
    }

    pub fn load<P: AsRef<Path>>(path: P) -> eyre::Result<Self> {
        let data = fs::read(path).context("failed to read key data from file")?;

        let cert = X509::from_pem(&data).context("failed to deserialize pem data")?;

        Ok(Self(cert))
    }
}

impl fmt::Debug for Cert {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let text = self.to_text().expect("failed to get text for certificate");

        let pem = self
            .to_pem()
            .expect("failed to get pem encoded certificate");

        write!(f, "{text}")?;
        write!(f, "{pem}")
    }
}

#[derive(Debug, Clone)]
pub struct Chain(Vec<Cert>);

impl Chain {
    pub fn new() -> Self {
        Self(Vec::new())
    }

    pub fn push(&mut self, cert: &Cert) {
        self.0.push(cert.clone());
    }

    pub fn append(&mut self, other: &mut Self) {
        self.0.append(&mut other.0);
    }

    pub fn load<P: AsRef<Path>>(path: P) -> eyre::Result<Self> {
        let file = File::open(path)?;
        let reader = BufReader::new(file);

        let mut line_buffer: Vec<_> = Vec::new();
        let mut certs: Vec<Cert> = Vec::new();

        for line in reader.lines() {
            let line = line?;

            if line.contains("BEGIN CERTIFICATE") {
                line_buffer = Vec::new();

                line_buffer.push(line.clone());

                continue;
            }

            if line.contains("END CERTIFICATE") {
                line_buffer.push(line.clone());

                let data = line_buffer.join("\n");

                let Ok(cert) = X509::from_pem(data.as_bytes()) else {
                    error!("invalid pem data in cert: {0}", certs.len());

                    continue;
                };

                certs.push(Cert(cert));

                line_buffer = Vec::new();

                continue;
            }

            line_buffer.push(line.clone());
        }

        Ok(Self(certs))
    }

    pub fn write<P: AsRef<Path>>(&self, path: P, perms: u32) -> eyre::Result<()> {
        let mut file = File::create(path)?;

        let mut permissions = file.metadata()?.permissions();
        permissions.set_mode(perms);
        file.set_permissions(permissions)?;

        for cert in &self.0 {
            let pem = cert.to_pem()?;

            file.write_all(pem.as_bytes())
                .context("failed to write key data to file")?;
        }

        Ok(())
    }
}

impl Deref for Chain {
    type Target = Vec<Cert>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
