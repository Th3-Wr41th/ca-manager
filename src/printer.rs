// SPDX-License-Identfier: MPL-2.0

use std::{fmt::Write, path::Path};

use bat::{
    assets::HighlightingAssets,
    config::Config,
    controller::Controller,
    style::{StyleComponent, StyleComponents},
    Input,
};
use clap::ValueEnum;
use color_eyre::eyre::{self, Context as _};
use dialoguer::console::Term;

use crate::{
    ssl::{Cert, Chain, Crl, KeyPair},
    Filenames,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq, ValueEnum, Default)]
pub enum Format {
    Plain,
    #[default]
    Pretty,
    Path,
}

pub struct Printer<'a> {
    config: Config<'a>,
    format: Format,
}

impl<'a> Printer<'a> {
    pub fn new(format: Format) -> Self {
        let config = match format {
            Format::Pretty | Format::Path => Config {
                colored_output: true,
                true_color: true,
                term_width: Term::stdout().size().1 as usize,
                style_components: StyleComponents::new(&[
                    StyleComponent::HeaderFilename,
                    StyleComponent::Grid,
                    StyleComponent::LineNumbers,
                ]),
                ..Default::default()
            },
            Format::Plain => Config {
                colored_output: true,
                true_color: true,
                term_width: Term::stdout().size().1 as usize,
                style_components: StyleComponents::new(&[
                    StyleComponent::HeaderFilename,
                    StyleComponent::Grid,
                ]),
                ..Default::default()
            },
        };

        Self { config, format }
    }

    pub fn print_chain<P: AsRef<Path>, B: Write>(
        &self,
        dir: P,
        text: bool,
        buffer: &mut B,
    ) -> eyre::Result<bool> {
        let path = dir.as_ref().join(Filenames::CHAIN);

        let bytes = if self.format == Format::Path {
            let val = path.to_string_lossy();

            val.as_bytes().to_vec()
        } else {
            info!("loading certificate chain");
            let chain = Chain::load(path).context("failed to load certificate chain")?;

            let mut certs: Vec<_> = Vec::new();

            for cert in &*chain {
                let data = if text {
                    [cert.to_text()?, cert.to_pem()?].join("\n")
                } else {
                    cert.to_pem()?.to_string()
                };

                certs.push(data);
            }

            let chain = certs.join("\n");

            chain.as_bytes().to_vec()
        };

        let st = self
            .print(
                vec![Input::from_bytes(&bytes)
                    .title(Filenames::CHAIN)
                    .kind("Chain")
                    .into()],
                buffer,
            )
            .context("failed to print certificate chain")?;

        Ok(st)
    }

    pub fn print_crl<P: AsRef<Path>, B: Write>(
        &self,
        dir: P,
        text: bool,
        buffer: &mut B,
    ) -> eyre::Result<bool> {
        let path = dir.as_ref().join(Filenames::CRL);

        let bytes = if self.format == Format::Path {
            let val = path.to_string_lossy();

            val.as_bytes().to_vec()
        } else {
            info!("loading certificate revocation list");
            let crl = Crl::load(path).context("failed to load crl")?;

            let data = if text {
                [crl.to_text()?, crl.to_pem()?].join("\n")
            } else {
                crl.to_pem()?.to_string()
            };

            data.as_bytes().to_vec()
        };

        let st = self
            .print(
                vec![Input::from_bytes(&bytes)
                    .title(Filenames::CRL)
                    .kind("Certificate Revocation List")
                    .into()],
                buffer,
            )
            .context("failed to print crl")?;

        Ok(st)
    }

    pub fn print_keypair<P: AsRef<Path>, B: Write>(
        &self,
        dir: P,
        buffer: &mut B,
        decrypt: bool,
    ) -> eyre::Result<bool> {
        let path = dir.as_ref().join(Filenames::KEY);

        let bytes = if self.format == Format::Path {
            let val = path.to_string_lossy();

            val.as_bytes().to_vec()
        } else {
            info!("load private key");
            let (key, encrypt_pass) = KeyPair::load(path).context("failed to private key")?;

            let private_key = if !decrypt & encrypt_pass.is_some() {
                key.private_key_to_pem_pkcs8_passphrase(encrypt_pass.unwrap().as_ref())?
            } else {
                key.private_key_to_pem_pkcs8()?
            };

            let public_key = key.public_key_to_pem()?;

            let pem = [private_key, public_key].join("\n");

            pem.as_bytes().to_vec()
        };

        let st = self
            .print(
                vec![Input::from_bytes(&bytes)
                    .title(Filenames::KEY)
                    .kind("Key Pair")
                    .into()],
                buffer,
            )
            .context("failed to print keypair")?;

        Ok(st)
    }

    pub fn print_cert<P: AsRef<Path>, B: Write>(
        &self,
        dir: P,
        text: bool,
        buffer: &mut B,
    ) -> eyre::Result<bool> {
        let path = dir.as_ref().join(Filenames::CERTIFICATE);

        let bytes = if self.format == Format::Path {
            let val = path.to_string_lossy();

            val.as_bytes().to_vec()
        } else {
            info!("loading certifcate");
            let cert = Cert::load(path).context("failed to load certificate")?;

            let data = if text {
                [cert.to_text()?, cert.to_pem()?].join("\n")
            } else {
                cert.to_pem()?.to_string()
            };

            data.as_bytes().to_vec()
        };

        let st = self
            .print(
                vec![Input::from_bytes(&bytes)
                    .title(Filenames::CERTIFICATE)
                    .kind("Certificate")
                    .into()],
                buffer,
            )
            .context("failed to print certificate")?;

        Ok(st)
    }

    pub fn print<B: Write>(
        &self,
        inputs: Vec<bat::input::Input>,
        buffer: &mut B,
    ) -> eyre::Result<bool> {
        let st = Controller::new(&self.config, &HighlightingAssets::from_binary())
            .run(inputs, Some(buffer))?;

        Ok(st)
    }
}
