// SPDX-License-Identfier: MPL-2.0

#![deny(clippy::correctness, clippy::suspicious)]
#![warn(clippy::complexity, clippy::perf, clippy::style, clippy::pedantic)]
#![allow(clippy::module_name_repetitions)]

#[macro_use]
extern crate tracing;

mod cli;
mod dialogue;
mod git;
mod printer;
mod ssl;
mod subcmds;
mod utilities;

use std::sync::Arc;

use clap::Parser;
use color_eyre::{
    config::HookBuilder,
    eyre::{self, Context as _},
};
use once_cell::sync::OnceCell;
use resolve_path::PathResolveExt as _;
use tracing::level_filters::LevelFilter;
use tracing_subscriber::{EnvFilter, FmtSubscriber};

use crate::cli::{CliArgs, Command};

static STORE: OnceCell<Arc<str>> = OnceCell::new();

pub struct Filenames;

impl Filenames {
    pub const CERTIFICATE: &'static str = "cert.pem";
    pub const KEY: &'static str = "privkey.pem";
    pub const CHAIN: &'static str = "chain.pem";
    pub const CRL: &'static str = "crl.pem";
}

fn init_error_hooks() -> eyre::Result<()> {
    let (panic, error) = HookBuilder::default().into_hooks();

    let panic = panic.into_panic_hook();
    let error = error.into_eyre_hook();

    color_eyre::eyre::set_hook(Box::new(move |err| error(err)))
        .context("failed to set eyre hook")?;

    std::panic::set_hook(Box::new(move |info| panic(info)));

    Ok(())
}

fn init_tracing_subscriber() -> eyre::Result<()> {
    let env_filter = {
        let default = EnvFilter::from_default_env();

        #[cfg(debug_assertions)]
        {
            default.add_directive(LevelFilter::DEBUG.into())
        }

        #[cfg(not(debug_assertions))]
        {
            default.add_directive(LevelFilter::WARN.into())
        }
    };

    let subscriber = FmtSubscriber::builder()
        .with_env_filter(env_filter)
        .finish();

    tracing::subscriber::set_global_default(subscriber)
        .context("setting default subscriber failed")?;

    info!("Tracing Started");

    Ok(())
}

fn main() -> eyre::Result<()> {
    init_error_hooks()?;
    init_tracing_subscriber()?;

    let args = CliArgs::parse();

    debug!("{args:#?}");

    let store: Arc<str> = {
        let tmp = args.store.as_ref();

        let resolve = tmp.resolve().to_path_buf();

        resolve.to_string_lossy().into()
    };

    let _store = STORE.get_or_init(|| store);

    match args.command {
        Command::Ca { command } => subcmds::ca::run(command)?,
        Command::New { args, name } => subcmds::new::run(args, name.as_ref())?,
        Command::Get(args) => subcmds::get::run(args)?,
        Command::Renew { args, name } => subcmds::renew::run(args, name.as_ref())?,
        Command::Revoke { name } => subcmds::revoke::run(name.as_ref())?,
        Command::Edit { name } => subcmds::edit::run(name.as_ref())?,
        Command::Check { name } => subcmds::check::run(name.as_ref())?,
    }

    todo!()
}
