// SPDX-License-Identfier: MPL-2.0

use std::{fmt, path::Path};

use color_eyre::eyre::{self, Context as _, OptionExt};
use dialoguer::Confirm;
use git2::{Commit, IndexAddOption, Oid, Repository};
use gpgme::{Context, Key, Protocol, SignMode};

use crate::{
    dialogue::{ConfirmExt, TERM},
    utilities::try_open,
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[allow(dead_code)]
pub enum Operation {
    Add,
    AddAll,
    Remove,
}

pub enum CommitMessage<'a> {
    Initial(&'a str),
    New(&'a str),
    Edit(&'a str),
    Renew(&'a str),
    Revoke(&'a str),
}

impl fmt::Display for CommitMessage<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = match self {
            Self::Initial(msg) => format!("init: {msg}"),
            Self::New(msg) => format!("new: {msg}"),
            Self::Edit(msg) => format!("edit: {msg}"),
            Self::Renew(msg) => format!("renew: {msg}"),
            Self::Revoke(msg) => format!("revoke: {msg}"),
        };

        write!(f, "{msg}")
    }
}

pub trait RepositoryExt {
    fn initialize<P: AsRef<Path> + std::fmt::Debug>(path: P) -> eyre::Result<Repository>;

    fn initial_commit(&self) -> eyre::Result<()>;

    fn get_signing_key(&self) -> Option<Key>;

    fn create_signed_commit(
        &self,
        key: Key,
        message: &CommitMessage,
        tree_id: Oid,
        parents: &[&Commit],
    ) -> eyre::Result<Oid>;

    fn create_commit(
        &self,
        message: &CommitMessage,
        tree_id: Oid,
        parents: &[&Commit],
    ) -> eyre::Result<Oid>;

    fn files<P: AsRef<Path>>(&self, paths: &[(P, Operation)]) -> eyre::Result<Oid>;

    fn commit_changes<P: AsRef<Path>>(
        &self,
        paths: &[(P, Operation)],
        message: &CommitMessage,
    ) -> eyre::Result<()>;
}

impl RepositoryExt for Repository {
    fn initialize<P: AsRef<Path> + std::fmt::Debug>(path: P) -> eyre::Result<Repository> {
        let repo = Self::init(&path).context("failed to initialize repository")?;

        let edit_config = Confirm::default_prompt("Do you want to edit the git config")
            .interact_on(&TERM)
            .context("failed to get confirmation from user")?;

        if edit_config {
            let path = path.as_ref().join(".git/config");

            try_open(path).context("failed to open git config")?;
        }

        Ok(repo)
    }

    fn initial_commit(&self) -> eyre::Result<()> {
        let tree_id = self.files(&[("*", Operation::AddAll)])?;

        let config = self.config().context("unable to get git config")?;

        let branch = config.get_str("init.defaultBranch").unwrap_or("master");

        let commit_id = match self.get_signing_key() {
            Some(key) => self.create_signed_commit(
                key,
                &CommitMessage::Initial("initialize ca store"),
                tree_id,
                &[],
            ),
            None => {
                self.create_commit(&CommitMessage::Initial("initialize ca store"), tree_id, &[])
            }
        }?;

        let commit = self
            .find_commit(commit_id)
            .context("cannot find initial commit in repo")?;

        self.branch(branch, &commit, false)
            .context("failed to create branch for initial commit")?;

        Ok(())
    }

    fn commit_changes<P: AsRef<Path>>(
        &self,
        paths: &[(P, Operation)],
        message: &CommitMessage<'_>,
    ) -> eyre::Result<()> {
        info!("getting HEAD for the repository");
        let head = self
            .head()
            .context("failed to get HEAD for the repository")?;

        let branch = head
            .shorthand()
            .ok_or_eyre("head reference contains non utf-8 characters ")?;

        info!("finding parent commit");

        let head_oid = head
            .target()
            .ok_or_eyre("HEAD does not appear to be a valid reference")?;

        let head_commit = self
            .find_commit(head_oid)
            .context("cannot find commit reference by HEAD")?;

        info!("adding files to index");
        let tree_id = self.files(paths)?;

        let commit_id = match self.get_signing_key() {
            Some(key) => self.create_signed_commit(key, message, tree_id, &[&head_commit])?,
            None => self.create_commit(message, tree_id, &[&head_commit])?,
        };

        self.reference(
            &format!("refs/heads/{branch}"),
            commit_id,
            true,
            &message.to_string(),
        )
        .context("failed to create reference for commit")?;

        Ok(())
    }

    fn files<P: AsRef<Path>>(&self, paths: &[(P, Operation)]) -> eyre::Result<Oid> {
        let mut index = self
            .index()
            .context("failed to get index file for repository")?;

        for (path, operation) in paths {
            let path: &Path = path.as_ref();

            match operation {
                Operation::Add => index.add_path(path).context(format!(
                    "failed to add path to index: {0}",
                    path.to_string_lossy()
                ))?,
                Operation::AddAll => index
                    .add_all(path.iter(), IndexAddOption::DEFAULT, None)
                    .context("failed to add paths to index")?,
                Operation::Remove => index.remove_path(path).context(format!(
                    "failed to remove path from index: {0}",
                    path.to_string_lossy(),
                ))?,
            }
        }

        index.write().context("failed to write index")?;

        index
            .write_tree()
            .context("failed to write index as a tree")
    }

    fn create_commit(
        &self,
        message: &CommitMessage<'_>,
        tree_id: Oid,
        parents: &[&Commit],
    ) -> eyre::Result<Oid> {
        info!("creating commit");
        let sig = self
            .signature()
            .context("failed to get signature for git repository")?;

        let tree = self.find_tree(tree_id).context("failed to find tree")?;

        self.commit(
            Some("HEAD"),
            &sig,
            &sig,
            &message.to_string(),
            &tree,
            parents,
        )
        .context("failed to create commit")
    }

    fn create_signed_commit(
        &self,
        key: Key,
        message: &CommitMessage<'_>,
        tree_id: Oid,
        parents: &[&Commit],
    ) -> eyre::Result<Oid> {
        info!("creating signed commit");
        let sig = self.signature().context("failed to get signature")?;

        let tree = self.find_tree(tree_id).context("failed to find tree")?;

        let buf = self
            .commit_create_buffer(&sig, &sig, &message.to_string(), &tree, parents)
            .context("failed to create commit buffer")?;

        let contents = buf.as_str().ok_or_eyre("commit buffer is invalid utf-8")?;

        let mut outbuf: Vec<_> = Vec::new();

        let mut ctx =
            Context::from_protocol(Protocol::OpenPgp).context("failed to create gpg context")?;

        ctx.add_signer(&key)
            .context("failed to add signing key to gpg context")?;
        ctx.set_armor(true);
        ctx.sign(SignMode::Detached, contents, &mut outbuf)
            .context("failed to sign commit buffer")?;

        let out = std::str::from_utf8(&outbuf).context("signed commit is invalid utf-8")?;

        self.commit_signed(contents, out, None)
            .context("failed to create signed commit")
    }

    fn get_signing_key(&self) -> Option<Key> {
        let mut config = self.config().ok()?;

        let config = config.snapshot().ok()?;

        if !config.get_bool("commit.gpgsign").ok()? {
            return None;
        }

        let signing_key = config.get_str("user.signingkey").ok()?;

        let mut ctx = Context::from_protocol(Protocol::OpenPgp).ok()?;

        let keys = ctx.find_keys([signing_key]).ok()?;

        let key = keys.filter_map(Result::ok).find(Key::can_sign)?;

        Some(key)
    }
}
