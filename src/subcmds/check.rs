// SPDX-License-Identfier: MPL-2.0

use color_eyre::eyre::{self, bail, Context as _};
use derive_builder::Builder;
use dialoguer::console::{style, Style};
use once_cell::sync::Lazy;
use openssl::{
    asn1::Asn1Time,
    x509::{CrlStatus, X509VerifyResult},
};
use std::{fmt, path::Path, rc::Rc};

use crate::{
    ssl::{Cert, Crl, KeyPair},
    Filenames, STORE,
};

static PREFIX_STYLE: Lazy<Style> = Lazy::new(|| Style::new().bold().white());

pub fn run(name: &str) -> eyre::Result<()> {
    info!("checking path");
    let Some(store) = STORE.get().map(AsRef::as_ref) else {
        bail!("store not initalized, aborting");
    };

    let store = Path::new(store);
    let ca_dir = store.join("ca");
    let dir = store.join(name);

    info!("loading certificate revocation list");
    let crl = Crl::load(ca_dir.join(Filenames::CRL)).context("failed to load crl")?;

    info!("loading ca certificate and keypair");
    let ca_cert =
        Cert::load(ca_dir.join(Filenames::CERTIFICATE)).context("failed to load ca certificate")?;
    let (ca_key, _) =
        KeyPair::load(ca_dir.join(Filenames::KEY)).context("failed to load ca private key")?;

    info!("loading certificate and keypair");
    let cert =
        Cert::load(dir.join(Filenames::CERTIFICATE)).context("failed to load certificate")?;
    let (_, encrypt_pass) =
        KeyPair::load(dir.join(Filenames::KEY)).context("failed to load private key")?;

    let mut report = ReportBuilder::default();

    {
        info!("checking certificate signature");

        let validity = if cert.verify(&ca_key)? {
            Validity::Valid
        } else {
            Validity::Invalid
        };

        report.ca_sig(validity);
    }

    {
        info!("checking certificate issuer");

        let validity = match ca_cert.issued(&cert) {
            X509VerifyResult::OK => Validity::Valid,
            _ => Validity::Invalid,
        };

        report.issuer(validity);
    }

    'root: {
        info!("checking certificate validity");

        if let CrlStatus::Revoked(revoked) = crl.is_revoked(&cert) {
            let when = revoked.revocation_date().to_string();

            report.validity(CertValidity::Revoked(when.into()));

            break 'root;
        }

        let cert = cert.inner();

        let now = Asn1Time::now()?;

        let not_before = cert.not_before();
        let not_after = cert.not_after();

        let validity = if now < not_before {
            let when = not_before.to_string();

            CertValidity::Early(when.into())
        } else if now > not_after {
            let when = not_after.to_string();

            CertValidity::Expired(when.into())
        } else {
            CertValidity::Valid
        };

        report.validity(validity);
    }

    {
        info!("checking private key security");

        let security = if encrypt_pass.is_some() {
            Security::Encrypted
        } else {
            Security::Unencrypted
        };

        report.security(security);
    }

    let report = report.build().context("failed to build report")?;

    let prefix = PREFIX_STYLE.apply_to("Checking");
    let msg = style(name);

    println!("{prefix}: {msg}");
    println!("{report}");

    Ok(())
}

#[derive(Debug, Clone, Builder)]
#[builder(name = "ReportBuilder")]
struct Report {
    ca_sig: Validity,
    issuer: Validity,
    validity: CertValidity,
    security: Security,
}

impl fmt::Display for Report {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let prefix = PREFIX_STYLE.apply_to("Certificate Authority Signature");
        let msg = self.ca_sig.to_string();

        writeln!(f, "{prefix}: {msg}")?;

        let prefix = PREFIX_STYLE.apply_to("Certificate Issuer");
        let msg = self.issuer.to_string();

        writeln!(f, "{prefix}: {msg}")?;

        let prefix = PREFIX_STYLE.apply_to("Certificate Validity");
        let msg = self.validity.to_string();

        writeln!(f, "{prefix}: {msg}")?;

        let prefix = PREFIX_STYLE.apply_to("Private Key");
        let msg = self.security.to_string();

        writeln!(f, "{prefix}: {msg}")
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Validity {
    Valid,
    Invalid,
}

impl fmt::Display for Validity {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = match self {
            Validity::Valid => style("Valid").green(),
            Validity::Invalid => style("Invalid").red(),
        };

        write!(f, "{msg}")
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum CertValidity {
    Early(Rc<str>),
    Valid,
    Expired(Rc<str>),
    Revoked(Rc<str>),
}

impl fmt::Display for CertValidity {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = match self {
            Self::Early(when) => style(format!("certificate not valid until: {when}")).red(),
            Self::Valid => style("Valid".to_string()).green(),
            Self::Expired(when) => style(format!("certificate expired: {when}")).red(),
            Self::Revoked(when) => style(format!("certificate revoked: {when}")).red(),
        };

        write!(f, "{msg}")
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Security {
    Encrypted,
    Unencrypted,
}

impl fmt::Display for Security {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = match self {
            Self::Encrypted => style("Encrypted").green(),
            Self::Unencrypted => style("Unencrypted").yellow(),
        };

        write!(f, "{msg}")
    }
}
