// SPDX-License-Identfier: MPL-2.0

use clap::ValueEnum;

use crate::ssl;

pub mod ca;
pub mod check;
pub mod edit;
pub mod get;
pub mod new;
pub mod renew;
pub mod revoke;

#[derive(Debug, Clone, Copy, ValueEnum)]
pub enum Algorithm {
    Rsa,
    Ed25519,
}

impl Algorithm {
    pub fn into_ssl(self, bits: u32) -> ssl::Algorithm {
        match self {
            Algorithm::Ed25519 => ssl::Algorithm::Ed25519,
            Algorithm::Rsa => ssl::Algorithm::Rsa(bits),
        }
    }
}
