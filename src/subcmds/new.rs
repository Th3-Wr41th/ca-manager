// SPDX-License-Identfier: MPL-2.0

use clap::Args;
use color_eyre::eyre::{self, bail, Context};
use dialoguer::Password;
use git2::Repository;
use openssl::asn1::Asn1Time;
use std::{
    fs::{DirBuilder, File},
    io::{BufRead, BufReader},
    os::unix::fs::DirBuilderExt,
    path::Path,
    sync::Arc,
};

use crate::{
    dialogue::{PasswordExt, TERM},
    git::{CommitMessage, Operation, RepositoryExt},
    ssl::{Cert, Chain, KeyPair, SubjectAltName},
    utilities::{parse_expiry_timespan, parse_x509_name_data},
    STORE,
};

use super::Algorithm;

#[derive(Debug, Clone, Args)]
pub struct NewArgsGen {
    #[arg(long, value_name = "SUBJ_DATA")]
    pub x509: Arc<str>,

    /// Which algorithm the use for the generated keypair
    #[arg(long, value_enum)]
    pub algorithm: Algorithm,

    /// The number of bits for an Algorithm::RSA keypair
    #[arg(long, default_value_t = 4096)]
    pub bits: u32,

    /// How long the certificate lasts
    #[arg(long)]
    pub expires: Arc<str>,

    /// Encrypt private key
    #[arg(long)]
    pub encrypt: bool,
}

#[derive(Debug, Clone, Args)]
pub struct NewArgs {
    #[command(flatten)]
    pub gen_args: NewArgsGen,

    /// Subject Alternative Names
    #[arg(long, value_name = "SUBJ_ALT")]
    pub subject_alt: Arc<str>,
}

pub fn run(args: NewArgs, name: &str) -> eyre::Result<()> {
    info!("generating new certificate store");

    let Some(store) = STORE.get().map(AsRef::as_ref) else {
        bail!("store not initalized, aborting");
    };

    let store_path = Path::new(store);

    let NewArgs {
        gen_args,
        subject_alt,
    } = args;

    let NewArgsGen {
        x509,
        algorithm,
        bits,
        expires,
        encrypt,
    } = gen_args;

    info!("loading ca certificate");
    let ca_cert =
        Cert::load(store_path.join("ca/cert.pem")).context("failed to load ca certificate")?;

    info!("loading ca key");
    let (ca_key, _) = {
        let path = store_path.join("ca/privkey.pem");

        let file = File::open(path).context("failed to open ca private key file")?;
        let mut reader = BufReader::new(file);

        let mut line = String::new();
        let read = reader
            .read_line(&mut line)
            .context("failed to read ca private key from file")?;

        if read == 0 {
            bail!("failed to read ca private key from file, file appears to be empty");
        }

        KeyPair::load(store_path.join("ca/privkey.pem")).context("failed to load ca private key")?
    };

    info!("generating key pair");
    let algo = algorithm.into_ssl(bits);
    let key_pair = KeyPair::generate(algo).context("failed to generate keypair")?;

    info!("parsing x509 name data");
    let name_data = parse_x509_name_data(&x509).context("failed to parse x509 name data")?;

    info!("parsing expiry timespan");
    let expiry_time = {
        let days = parse_expiry_timespan(&expires).context("failed to parse expiry timespan")?;

        Asn1Time::days_from_now(days)?
    };

    let subject_alt = SubjectAltName::from_str(subject_alt.as_ref());

    info!("generating certificate");
    let cert = Cert::mk_ca_signed_cert(
        &ca_cert,
        &ca_key,
        &key_pair,
        &name_data,
        &expiry_time,
        subject_alt,
    )
    .context("failed to generate certificate")?;

    let mut chain = Chain::load(store_path.join("ca/chain.pem"))
        .context("failed to load root certificate chain")?;

    chain.push(&cert);

    info!("getting private key password");
    let pass: Option<String> = if encrypt {
        let pass = Password::default_ext()
            .with_prompt("Password")
            .with_confirmation("Confirm Password", "Passwords do not match")
            .interact_on(&TERM)
            .context("failed to get private key password from user")?;

        Some(pass)
    } else {
        None
    };

    let dir = store_path.join(name);

    DirBuilder::new()
        .mode(0o750)
        .recursive(true)
        .create(&dir)
        .context("failed to create directory")?;

    key_pair
        .write(dir.join("privkey.pem"), pass.as_deref(), false, 0o640)
        .context("failed to write keypair")?;

    cert.write(dir.join("cert.pem"), 0o640)
        .context("failed to write certificate")?;

    chain
        .write(dir.join("chain.pem"), 0o640)
        .context("failed to write certificate chain")?;

    let repo = Repository::open(store_path).context("failed to open repository")?;

    repo.commit_changes(
        &[(name, Operation::AddAll)],
        &CommitMessage::New(format!("add certificate store for {name}").as_str()),
    )
    .context("failed to commit changes")?;

    Ok(())
}
