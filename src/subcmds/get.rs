// SPDX-License-Identfier: MPL-2.0

use clap::Args;
use color_eyre::eyre::{self, bail};
use std::{path::Path, sync::Arc};

use crate::{
    printer::{Format, Printer},
    STORE,
};

#[derive(Debug, Clone, Args)]
#[allow(clippy::struct_excessive_bools)]
pub struct GetArgs {
    /// Get the certificate chain
    #[arg(long)]
    pub chain: bool,

    /// Get the private key
    #[arg(long)]
    pub key: bool,

    /// Get the certificate (default)
    #[arg(long)]
    pub cert: bool,

    /// Output format
    #[arg(short, long, value_enum, default_value_t = Format::default())]
    pub format: Format,

    /// Prints out the certificate in text form
    #[arg(short, long)]
    pub text: bool,

    /// Decrypt the private key
    #[arg(short, long)]
    pub decrypt: bool,

    /// Name of certificate with optional sub directories
    pub name: Arc<str>,
}

pub fn run(mut args: GetArgs) -> eyre::Result<()> {
    info!("checking path");
    let Some(store) = STORE.get().map(AsRef::as_ref) else {
        bail!("store not initalized, aborting");
    };

    let dir = Path::new(store).join(args.name.as_ref());

    if !dir.exists() {
        bail!("cannot find directory by that name, aborting");
    }

    let printer = Printer::new(args.format);
    let mut output_buffer: Vec<String> = Vec::new();

    #[allow(clippy::needless_bitwise_bool)]
    if !args.chain & !args.key {
        args.cert = true;
    }

    if args.chain {
        let mut buffer = String::new();

        printer.print_chain(&dir, args.text, &mut buffer)?;

        output_buffer.push(buffer);
    }

    if args.key {
        let mut buffer = String::new();

        printer.print_keypair(&dir, &mut buffer, args.decrypt)?;

        output_buffer.push(buffer);
    }

    if args.cert {
        let mut buffer = String::new();

        printer.print_cert(&dir, args.text, &mut buffer)?;

        output_buffer.push(buffer);
    }

    let output = output_buffer.join("");

    println!("{output}");

    Ok(())
}
