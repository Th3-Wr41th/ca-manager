// SPDX-License-Identfier: MPL-2.0

use clap::Args;
use color_eyre::eyre::{self, bail, Context as _};
use git2::Repository;
use std::{path::Path, sync::Arc};

use crate::{
    git::{CommitMessage, Operation, RepositoryExt as _},
    ssl::{Cert, Chain, KeyPair},
    utilities::parse_expiry_timespan,
    Filenames, STORE,
};

#[derive(Debug, Clone, Args)]
pub struct RenewArgs {
    /// How long the certificate lasts
    #[arg(short, long)]
    pub expires: Arc<str>,
}

#[allow(clippy::needless_pass_by_value)]
pub fn run(args: RenewArgs, name: &str) -> eyre::Result<()> {
    info!("checking path");
    let Some(store) = STORE.get().map(AsRef::as_ref) else {
        bail!("store not initalized, aborting");
    };

    let store = Path::new(store);
    let dir = store.join(name);

    if !dir.exists() {
        bail!("cannot find directory by that name, aborting");
    }

    info!("loading ca certificate and keypair");
    let ca_cert = Cert::load(store.join("ca").join(Filenames::CERTIFICATE))
        .context("failed to load ca certificate")?;
    let (ca_key, _) = KeyPair::load(store.join("ca").join(Filenames::KEY))
        .context("failed to load ca private key")?;

    info!("loading certificate and keypair");
    let cert =
        Cert::load(dir.join(Filenames::CERTIFICATE)).context("failed to load certificate")?;
    let (key, _) = KeyPair::load(dir.join(Filenames::KEY)).context("failed to load keypair")?;

    let expires = parse_expiry_timespan(args.expires.as_ref())?;

    info!("renewing certificate");
    let new_cert = Cert::renew_cert(&ca_cert, &ca_key, &cert, &key, expires)
        .context("failed to renew certificate")?;

    info!("updating certificate chain");
    let mut chain = Chain::new();

    chain.push(&ca_cert);
    chain.push(&new_cert);

    info!("writing certificate to disk");
    new_cert
        .write(dir.join(Filenames::CERTIFICATE), 0o640)
        .context("failed to write certificate")?;

    info!("writing certificate chain to disk");
    chain
        .write(dir.join(Filenames::CHAIN), 0o640)
        .context("failed to write certificate chain")?;

    let repo = Repository::open(store).context("failed to open repository")?;

    repo.commit_changes(
        &[(name, Operation::AddAll)],
        &CommitMessage::Renew(format!("certificate for {name}").as_str()),
    )
    .context("failed to commit changes")?;

    Ok(())
}
