// SPDX-License-Identfier: MPL-2.0

use clap::{Args, Subcommand};
use color_eyre::eyre::{self, bail, Context as _};
use dialoguer::Password;
use git2::Repository;
use std::{fs::DirBuilder, os::unix::fs::DirBuilderExt as _, path::Path, sync::Arc};

use crate::{
    dialogue::{PasswordExt as _, TERM},
    git::RepositoryExt as _,
    ssl::{Cert, Chain, Crl, KeyPair},
    subcmds::new::NewArgsGen,
    utilities::{parse_expiry_timespan, parse_x509_name_data},
    Filenames, STORE,
};

#[derive(Debug, Clone, Args)]
pub struct CAInitArgs {
    #[command(subcommand)]
    pub command: Option<CAInitSubcommand>,

    #[command(flatten)]
    pub args: NewArgsGen,
}

#[derive(Debug, Clone, Subcommand)]
pub enum CAInitSubcommand {
    /// Initalize as an intermediate certificate authority
    Intermediate {
        /// Path to the ca private key
        #[arg(long)]
        ca_key: Arc<str>,

        /// Path to the ca certificate
        #[arg(long)]
        ca_cert: Arc<str>,

        /// Path to the ca certificate chain
        #[arg(long)]
        chain: Option<Arc<str>>,
    },
}

pub fn run(args: CAInitArgs) -> eyre::Result<()> {
    info!("initializing ca store");

    info!("checking path");
    let Some(store) = STORE.get().map(AsRef::as_ref) else {
        bail!("store not initalized, aborting");
    };

    if Path::new(store).exists() {
        bail!("store alreay exists, not overwriting");
    }

    let NewArgsGen {
        x509,
        algorithm,
        bits,
        expires,
        ..
    } = args.args;

    let ca_dir = Path::new(store).join("ca");

    let algo = algorithm.into_ssl(bits);

    info!("generating key pair");
    let key_pair = KeyPair::generate(algo).context("failed to generate ca keypair")?;

    info!("parsing x509 name data");
    let name_data = parse_x509_name_data(&x509)?;

    info!("parsing expiry timespan");
    let expiry_time = parse_expiry_timespan(&expires)?;

    let mut chain: Chain = Chain::new();

    let cert = if let Some(CAInitSubcommand::Intermediate {
        ca_key,
        ca_cert,
        chain: chain_file,
    }) = args.command
    {
        info!("loading ca key");
        let (ca_key, _) = KeyPair::load(ca_key.as_ref()).context("failed to load ca key")?;

        info!("loading ca certificate");
        let ca_cert = Cert::load(ca_cert.as_ref()).context("failed to to ca certificate")?;

        if let Some(chain_file) = chain_file {
            info!("loading certificate chain");
            let mut root_chain =
                Chain::load(chain_file.as_ref()).context("failed to load certificate chain")?;

            chain.append(&mut root_chain);
        } else {
            chain.push(&ca_cert);
        }

        info!("generating certificate");
        Cert::mk_ca_cert(
            &key_pair,
            &name_data,
            expiry_time,
            Some((&ca_cert, &ca_key)),
        )
        .context("failed to generate ca certificate")?
    } else {
        info!("generating certificate");
        Cert::mk_ca_cert(&key_pair, &name_data, expiry_time, None)
            .context("failed to generate ca certificate")?
    };

    chain.push(&cert);

    info!("generating crl");
    let crl = Crl::new(&cert, &key_pair).context("failed to generate crl")?;

    info!("getting private key password");
    let pass: Option<String> = if args.args.encrypt {
        let pass = Password::default_ext()
            .with_prompt("Password")
            .with_confirmation("Confirm Password", "Passwords do not match")
            .interact_on(&TERM)
            .context("failed to get private key password from user")?;

        Some(pass)
    } else {
        None
    };

    info!("initializing git repo in config dir");
    let repo = Repository::initialize(store)
        .context("failed to initialize git repository in config directory")?;

    DirBuilder::new()
        .mode(0o700)
        .create(&ca_dir)
        .context("failed to create ca directory")?;

    key_pair
        .write(ca_dir.join(Filenames::KEY), pass.as_deref(), false, 0o600)
        .context("failed to write ca keypair")?;

    cert.write(ca_dir.join(Filenames::CERTIFICATE), 0o600)
        .context("failed to write ca certificate")?;

    chain
        .write(ca_dir.join(Filenames::CHAIN), 0o600)
        .context("failed to write ca certificate chain")?;

    crl.write(ca_dir.join(Filenames::CRL), 0o600)
        .context("failed to write ca crl")?;

    repo.initial_commit()
        .context("failed to create initial commit")
}
