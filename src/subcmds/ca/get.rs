// SPDX-License-Identfier: MPL-2.0

use std::path::Path;

use clap::Args;
use color_eyre::eyre::{self, bail};

use crate::{
    printer::{Format, Printer},
    STORE,
};

#[derive(Debug, Clone, Args)]
#[allow(clippy::struct_excessive_bools)]
pub struct GetArgs {
    /// Get the ca certificate chain
    #[arg(long)]
    pub chain: bool,
    /// Get the certificate revocation list
    #[arg(long)]
    pub crl: bool,
    /// Get the ca private key
    #[arg(long)]
    pub key: bool,
    /// Get the ca certificate (default)
    #[arg(long)]
    pub cert: bool,

    /// Output format
    #[arg(short, long, value_enum, default_value_t = Format::default())]
    pub format: Format,

    /// Print out the certificate(s) in text form
    #[arg(short, long)]
    pub text: bool,

    /// Decrypt the private key
    #[arg(short, long)]
    pub decrypt: bool,
}

pub fn run(mut args: GetArgs) -> eyre::Result<()> {
    info!("checking path");
    let Some(store) = STORE.get().map(AsRef::as_ref) else {
        bail!("store not initalized, aborting");
    };

    let ca_dir = Path::new(store).join("ca");

    if !ca_dir.exists() {
        bail!("store does not exist or ca is not initialized, aborting");
    }

    let printer = Printer::new(args.format);
    let mut output_buffer: Vec<String> = Vec::new();

    #[allow(clippy::needless_bitwise_bool)]
    if !args.chain & !args.crl & !args.key {
        args.cert = true;
    }

    if args.chain {
        let mut buffer = String::new();

        printer.print_chain(&ca_dir, args.text, &mut buffer)?;

        output_buffer.push(buffer);
    }

    if args.crl {
        let mut buffer = String::new();

        printer.print_crl(&ca_dir, args.text, &mut buffer)?;

        output_buffer.push(buffer);
    }

    if args.key {
        let mut buffer = String::new();

        printer.print_keypair(&ca_dir, &mut buffer, args.decrypt)?;

        output_buffer.push(buffer);
    }

    if args.cert {
        let mut buffer = String::new();

        printer.print_cert(&ca_dir, args.text, &mut buffer)?;

        output_buffer.push(buffer);
    }

    let output = output_buffer.join("");

    println!("{output}");

    Ok(())
}
