// SPDX-License-Identfier: MPL-2.0

use clap::Subcommand;
use color_eyre::eyre;

use self::{get::GetArgs, init::CAInitArgs};
use super::renew::RenewArgs;

mod get;
mod init;
mod renew;

#[derive(Debug, Clone, Subcommand)]
pub enum CaCommands {
    /// Initialize a new certificate authority store
    Init(CAInitArgs),
    /// Get the ca certificate
    Get(GetArgs),
    /// Renew the ca certificate
    Renew(RenewArgs),
}

pub fn run(cmd: CaCommands) -> eyre::Result<()> {
    match cmd {
        CaCommands::Init(args) => init::run(args),
        CaCommands::Get(args) => get::run(args),
        CaCommands::Renew(args) => renew::run(args),
    }
}
