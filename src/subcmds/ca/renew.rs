// SPDX-License-Identfier: MPL-2.0

use std::path::Path;

use color_eyre::eyre::{self, bail, Context as _};
use git2::Repository;
use openssl::asn1::Asn1Time;

use crate::{
    git::{CommitMessage, Operation, RepositoryExt as _},
    ssl::{Cert, Chain, KeyPair},
    subcmds::renew::RenewArgs,
    utilities::parse_expiry_timespan,
    Filenames, STORE,
};

#[allow(clippy::needless_pass_by_value)]
pub fn run(args: RenewArgs) -> eyre::Result<()> {
    info!("checking path");
    let Some(store) = STORE.get().map(AsRef::as_ref) else {
        bail!("store not initalized, aborting");
    };

    let store = Path::new(store);
    let dir = store.join("ca");

    if !dir.exists() {
        bail!("cannot find directory by that name, aborting");
    }

    info!("loading ca certificate and keypair");
    let ca_cert = Cert::load(store.join("ca").join(Filenames::CERTIFICATE))
        .context("failed to load ca certificate")?;
    let (ca_key, _) = KeyPair::load(store.join("ca").join(Filenames::KEY))
        .context("failed to load ca private key")?;

    let expires = parse_expiry_timespan(args.expires.as_ref())?;
    let expires = Asn1Time::days_from_now(expires)?;

    info!("renewing ca certificate");
    let new_cert = Cert::renew_ca_cert(&ca_cert, &ca_key, &expires)
        .context("failed to renew ca certificate")?;

    info!("updating certificate chain");
    let mut chain = Chain::new();

    chain.push(&new_cert);

    info!("writing certificate to disk");
    new_cert
        .write(dir.join(Filenames::CERTIFICATE), 0o640)
        .context("failed to write certificate")?;

    info!("writing certificate chain to disk");
    chain
        .write(dir.join(Filenames::CHAIN), 0o640)
        .context("failed to write certificate chain")?;

    let repo = Repository::open(store).context("failed to open repository")?;

    repo.commit_changes(
        &[("ca", Operation::AddAll)],
        &CommitMessage::Renew("ca certificate"),
    )
    .context("failed to commit changes")?;

    todo!()
}
