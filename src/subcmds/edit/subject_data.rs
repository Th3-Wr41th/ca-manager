// SPDX-License-Identfier: MPL-2.0

use std::rc::Rc;

use color_eyre::eyre::{self, Context as _};
use openssl::{
    nid::Nid,
    x509::{X509Name, X509Ref},
};
use serde::{Deserialize, Serialize};

use crate::ssl::{Cert, SubjectAltName};

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct SubjectData {
    #[serde(rename = "subjectAltName")]
    pub alt_name: Option<Rc<str>>,

    pub expires: Option<Rc<str>>,

    pub subject_name: SubjectNameData,
}

impl SubjectData {
    pub fn from_cert(cert: &Cert) -> eyre::Result<Self> {
        let subject_name = SubjectNameData::from_cert(cert.inner())
            .context("failed to parse subject name data")?;

        let alt_name = SubjectAltName::from_cert(cert.inner())
            .context("failed to parse subject alternative names")?;

        let alt_name = alt_name.as_str();

        Ok(Self {
            alt_name: Some(alt_name),
            expires: None,
            subject_name,
        })
    }
}

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct SubjectNameData {
    #[serde(rename = "countryName")]
    pub country_name: Option<Rc<str>>,
    #[serde(rename = "stateOrProvinceName")]
    pub state_name: Option<Rc<str>>,
    #[serde(rename = "localityName")]
    pub locality: Option<Rc<str>>,
    #[serde(rename = "organizationName")]
    pub organization_name: Option<Rc<str>>,
    #[serde(rename = "organizationalUnitName")]
    pub organization_unit: Option<Rc<str>>,
    #[serde(rename = "commonName")]
    pub common_name: Option<Rc<str>>,
    #[serde(rename = "emailAddress")]
    pub email: Option<Rc<str>>,
}

impl SubjectNameData {
    pub fn from_cert(cert: &X509Ref) -> eyre::Result<Self> {
        let entries_iter = cert.subject_name().entries();

        let mut builder = Self::default();

        for entry in entries_iter {
            let nid = entry.object().nid();
            let data = entry.data().as_utf8()?;
            let data: &str = &data;

            match nid {
                Nid::COUNTRYNAME => builder.country_name = Some(data.into()),
                Nid::STATEORPROVINCENAME => builder.state_name = Some(data.into()),
                Nid::LOCALITYNAME => builder.locality = Some(data.into()),
                Nid::ORGANIZATIONNAME => builder.organization_name = Some(data.into()),
                Nid::ORGANIZATIONALUNITNAME => builder.organization_unit = Some(data.into()),
                Nid::COMMONNAME => builder.common_name = Some(data.into()),
                Nid::PKCS9_EMAILADDRESS => builder.email = Some(data.into()),
                _ => (),
            }
        }

        Ok(builder)
    }

    pub fn as_x509(&self) -> eyre::Result<X509Name> {
        let mut builder = X509Name::builder()?;

        if let Some(val) = &self.country_name {
            builder.append_entry_by_nid(Nid::COUNTRYNAME, val.as_ref())?;
        }

        if let Some(val) = &self.state_name {
            builder.append_entry_by_nid(Nid::STATEORPROVINCENAME, val.as_ref())?;
        }

        if let Some(val) = &self.locality {
            builder.append_entry_by_nid(Nid::LOCALITYNAME, val.as_ref())?;
        }

        if let Some(val) = &self.organization_name {
            builder.append_entry_by_nid(Nid::ORGANIZATIONNAME, val.as_ref())?;
        }

        if let Some(val) = &self.organization_unit {
            builder.append_entry_by_nid(Nid::ORGANIZATIONALUNITNAME, val.as_ref())?;
        }

        if let Some(val) = &self.common_name {
            builder.append_entry_by_nid(Nid::COMMONNAME, val.as_ref())?;
        }

        if let Some(val) = &self.email {
            builder.append_entry_by_nid(Nid::PKCS9_EMAILADDRESS, val.as_ref())?;
        }

        Ok(builder.build())
    }
}
