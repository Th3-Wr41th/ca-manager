// SPDX-License-Identfier: MPL-2.0

use std::path::Path;

use color_eyre::eyre::{self, bail, Context as _};
use git2::Repository;

use crate::{
    git::{CommitMessage, Operation, RepositoryExt as _},
    ssl::{Cert, Crl},
    Filenames, STORE,
};

pub fn run(name: &str) -> eyre::Result<()> {
    info!("checking path");
    let Some(store) = STORE.get().map(AsRef::as_ref) else {
        bail!("store not initalized, aborting");
    };

    let store_dir = Path::new(store);
    let ca_dir = store_dir.join("ca");
    let dir = store_dir.join(name);

    if !ca_dir.exists() {
        bail!("store does not exist or ca is not initialized, aborting");
    }

    if !dir.exists() {
        bail!("cannot find directory by that name, aborting");
    }

    info!("loading certificate revocation list");
    let mut crl = Crl::load(ca_dir.join(Filenames::CRL)).context("failed to load crl")?;

    info!("loading certificate");
    let cert =
        Cert::load(dir.join(Filenames::CERTIFICATE)).context("failed to load certificate")?;

    info!("revoking certificate");
    crl.revoke(&cert)?;

    info!("writing certificate revocation list to disk");
    crl.write(ca_dir.join(Filenames::CRL), 0o600)
        .context("failed to write crl")?;

    let repo = Repository::open(store).context("failed to open repository")?;

    repo.commit_changes(
        &[("ca", Operation::AddAll)],
        &CommitMessage::Revoke(format!("certificate for {name}").as_str()),
    )
    .context("failed to commit changes")?;

    Ok(())
}
