// SPDX-License-Identfier: MPL-2.0

mod subject_data;

use chrono::NaiveDateTime;
use color_eyre::eyre::{self, bail, Context};
use dialoguer::Editor;
use git2::Repository;
use openssl::asn1::Asn1Time;
use std::path::Path;

use crate::{
    git::{CommitMessage, Operation, RepositoryExt as _},
    ssl::{Cert, Chain, KeyPair, SubjectAltName},
    utilities::parse_expiry_timespan,
    Filenames, STORE,
};

use self::subject_data::SubjectData;

pub fn run(name: &str) -> eyre::Result<()> {
    info!("checking path");
    let Some(store) = STORE.get().map(AsRef::as_ref) else {
        bail!("store not initalized, aborting");
    };

    let store = Path::new(store);
    let dir = store.join(name);

    if !dir.exists() {
        bail!("cannot find directory by that name, aborting");
    }

    info!("loading ca certificate and keypair");
    let ca_cert = Cert::load(store.join("ca").join(Filenames::CERTIFICATE))
        .context("failed to load ca certificate")?;
    let (ca_key, _) = KeyPair::load(store.join("ca").join(Filenames::KEY))
        .context("failed to load ca private key")?;

    info!("loading certificate and keypair");
    let cert =
        Cert::load(dir.join(Filenames::CERTIFICATE)).context("failed to load certificate")?;
    let (key, _) = KeyPair::load(dir.join(Filenames::KEY)).context("failed to load keypair")?;

    info!("parsing certificate data");

    let data = SubjectData::from_cert(&cert).context("failed to parse certificate")?;

    let toml = toml::to_string_pretty(&data)?;

    info!("opening editor");
    let Some(edited) = Editor::new().extension(".toml").edit(&toml)? else {
        info!("nothing changed, aborting");

        return Ok(());
    };

    let new: SubjectData = toml::from_str(&edited)?;

    info!("parsing expiry timespan");
    let expires = if let Some(time) = new.expires {
        let days =
            parse_expiry_timespan(time.as_ref()).context("failed to parse expiry timespan")?;

        Asn1Time::days_from_now(days)?
    } else {
        let future = cert.inner().not_after();
        let str = future.to_string();

        let datetime = NaiveDateTime::parse_from_str(&str, "%b %e %X %Y %Z")?;
        let datetime = format!("{0}", datetime.format("%Y%m%d%H%M%SZ"));

        Asn1Time::from_str(&datetime)?
    };

    info!("parsing subject name data");
    let name_data = new.subject_name.as_x509()?;

    info!("parsing subject alternative names");
    let subject_alt_name = if let Some(alt_name) = new.alt_name {
        SubjectAltName::from_str(alt_name.as_ref())
    } else {
        SubjectAltName::from_cert(cert.inner())?
    };

    info!("regenerating certificate");
    let new_cert = Cert::mk_ca_signed_cert(
        &ca_cert,
        &ca_key,
        &key,
        &name_data,
        &expires,
        subject_alt_name,
    )
    .context("failed to regenerate certificate")?;

    info!("updating certificate chain");
    let mut chain = Chain::new();

    chain.push(&ca_cert);
    chain.push(&new_cert);

    info!("writing certificate to disk");
    new_cert
        .write(dir.join(Filenames::CERTIFICATE), 0o640)
        .context("failed to write certificate")?;

    info!("writing certificate chain to disk");
    chain
        .write(dir.join(Filenames::CHAIN), 0o640)
        .context("failed to write certificate chain")?;

    info!("committing changes");
    let repo = Repository::open(store).context("failed to open repository")?;

    repo.commit_changes(
        &[(name, Operation::AddAll)],
        &CommitMessage::Edit(format!("certificate store for {name}").as_str()),
    )
    .context("failed to commit changes")?;

    Ok(())
}
