# Changelog

All changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.1.0] - 2024-04-01 

### Documentation

- **3a25a20** - add changelog notice and license notice to readme
- **3d2827e** - add changelog
- **bb2d0e0** - add todo and project description to readme
- **c9497de** - update readme to reflect implemented structure
- **8e7ff86** - update readme to reflect implemented structure
- **13c117b** - update readme to reflect implemented structure
- **9167fd7** - update readme with the revised structure

### Code Refactoring

- **950bc7b** - add visability modifiers to argument stuctures
- **c14859c** - move store to global static
- **4caec05** - move subcommands into distinct modules
- **41321a5** - clean up p.o.c code

### Chores

- **215295f** - bump version to 0.1.0

### Features

- **0d6e99a** - implement git functionality
- **5260007** - impl support for encrypted private keys
- **26141d9** - impl 'get' subcommand
- **6b362a1** - impl 'ca get' subcommand
- **c488757** - automatically detect encypted keys
- **6c63357** - impl support for generating crl during ca init
- **5186a88** - impl 'new' subcommand
- **4f5c7d1** - revise method to calculating expiry date and add tests to verify
- **4fe6438** - impl ca init subcommand
- **382ecaf** - impl cli structure
- **0ff54a8** - initial commit

[Unreleased]: https://gitlab.com/Th3-Wr41th/ca-manager/compare/v0.1.0...HEAD
[v0.1.0]: https://gitlab.com/Th3-Wr41th/ca-manager/commits/v0.1.0
